#!/bin/bash
mkdir -p ~/projects/stam/unit-test
code ~/projects/stam/unit-test

git init
npm init -y

npm i --save express # or npm install --save express
npm i --save-dev mocha chai

echo "node_modules" > .gitignore

touch README.md
mkdir src test
touch src/main.js src/calc.js
mkdir public
touch public/index.html
touch test/calc.test.js

git remote add origin https://gitlab.com/virvale_lab/unit-test

# test files that goes to staging area
git add . --dry-run

git commit -m "[message]"
git push -u origin main
