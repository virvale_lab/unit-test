/**
 * Adds two numbers together.
 * @param {number} a number 1
 * @param {number} b number 2
 * @returns {number}
 */
const add = (a, b) => a + b;

/**
 * Subtracts b from a.
 * @param {number} minuend
 * @param {number} subtrahend subtrahend
 * @returns {number}
 */
const subtract = (minuend, subtrahend) => minuend - subtrahend;

/**
 * Multiplies multiplicant with multiplier.
 * @param {number} multiplier
 * @param {number} multiplicant
 * @returns {number}
 */
const multiply = (multiplier, multiplicant) => multiplier * multiplicant;

/**
 * Divides dividend with divisor.
 * @param {number} dividend 
 * @param {number} divisor 
 * @returns {number}
 * @throws {Error} 0 division
 */
const divide = (dividend, divisor) => {
    if (divisor == 0) throw new Error("0 division not allowed")
    const fraction = dividend / divisor;
    return fraction;
}

export default { add, subtract, multiply, divide }